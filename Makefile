
all:
	pyuic5 designer/MainWindow.ui > designer/Ui_MainWindow.py
	pyuic5 designer/Stepper.ui > designer/Ui_Stepper.py
	pyuic5 designer/Discrets.ui > designer/Ui_Discrets.py
	pyuic5 designer/Register595Leds.ui > designer/Ui_Register595Leds.py
	pyuic5 designer/Register595Config.ui > designer/Ui_Register595Config.py

