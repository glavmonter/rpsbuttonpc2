# -*- coding: utf-8 -*-
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QApplication
import logging


if __name__ == '__main__':
    import sys

    logging.basicConfig(format='[%(name)-10s] [%(levelname)-8s] [%(asctime)s] %(message)s',
                        datefmt='%Y-%m-%d, %H:%M:%S', filename='logging.log')

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    formatter = logging.Formatter(fmt='[%(name)-10s] [%(levelname)-8s] [%(asctime)s] %(message)s',
                                  datefmt='%Y-%m-%d, %H:%M:%S')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    logger = logging.getLogger('RPSButtonPC')
    logger.setLevel(logging.DEBUG)
    logger.debug("Hello")
    # QCoreApplication.setOrganizationName('RPS')
    # QCoreApplication.setApplicationName('Button Test Program')
    # app = QApplication(sys.argv)
    # window = MainWindow()
    # window.show()
    # sys.exit(app.exec_())
    sys.exit(0)
